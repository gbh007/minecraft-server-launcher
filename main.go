package main

import (
	"app/handlers"
	"app/server"
	"app/storage"
	"app/storage/csvStorage"
	"app/storage/memoryStorage"
	"context"
	"flag"
	"log"
	"os/signal"
	"syscall"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	cheatsMode := flag.Bool("cheats", false, "активировать читы")
	fname := flag.String("fname", "server.jar", "название файла для запуска")
	msSize := flag.String("ms", "1G", "размер памяти для JWM")
	jvmPath := flag.String("jvm-path", "/usr/lib/jvm/jdk-17/bin/java", "путь до JVM")
	storType := flag.String("stor", "csv", "Используемое хранилище данных - csv/mem")
	flag.Parse()

	ctx, cancelNotify := signal.NotifyContext(
		context.Background(),
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)
	defer cancelNotify()

	var stor storage.Storage

	switch *storType {
	case "csv":
		stor = csvStorage.New()

	default:
		stor = memoryStorage.New()
	}

	// Создаем и конфигурируем обработчики данных
	stdIOHandler := handlers.NewStdIO()
	cordSearchHandler := handlers.NewCordSearcher()
	botHandler := handlers.NewBot(stor)

	botHandler.Cheats = *cheatsMode
	botHandler.RegisterCorder(cordSearchHandler.Search)

	// Конфигурируем сервер
	cfg := server.ServerConfig{
		Xmx:      *msSize,
		Xms:      *msSize,
		JVMPath:  *jvmPath,
		Filename: *fname,
	}

	// Создаем сервер
	srv := server.NewServer(cfg)
	srv.RegisterHandler(stdIOHandler)
	srv.RegisterHandler(cordSearchHandler)
	srv.RegisterHandler(botHandler)

	// Запускаем асинхронные процессы
	go stdIOHandler.Run(ctx)

	err := srv.Run(ctx)
	if err != nil {
		log.Println(err)
	}
}
