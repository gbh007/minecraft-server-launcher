create_build_dir:
	mkdir -p ./_build

build: create_build_dir
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 go build -o ./_build/launcher-arm64 .
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./_build/launcher-amd64 .