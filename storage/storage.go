package storage

// Storage - интерфейс хранилища.
type Storage interface {
	// SetUserTPInfo - устанавливает данные телепорта игрока.
	SetUserTPInfo(info UserTPInfo) bool
	// Получает данные телепорта игрока по названию.
	GetUserTPInfo(user, alias string) (UserTPInfo, bool)
	// Получает данные телепортов игрока.
	GetUserTPInfos(user string) []UserTPInfo
	// DelUserTPInfo - удаляет данные телепорта игрока.
	DelUserTPInfo(user, alias string) bool

	// GetBuffInfo - получает данные эффекта по внутреннему коду.
	GetBuffInfo(code string) []BuffInfo
	// GetBuffListInfo - получает данные эффектов.
	GetBuffListInfo() []BuffShortInfo

	// SetUserAliasInfo - устанавливает данные алиаса игрока.
	SetUserAliasInfo(info UserAliasInfo) bool
	// Получает данные алиаса игрока по названию.
	GetUserAliasInfo(user, alias string) (UserAliasInfo, bool)
	// Получает данные алиасов игрока.
	GetUserAliasInfoList(user string) []UserAliasInfo
	// DelUserAliasInfo - удаляет данные алиаса игрока.
	DelUserAliasInfo(user, alias string) bool
}
