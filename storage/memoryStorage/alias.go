package memoryStorage

import "app/storage"

// SetUserAliasInfo - устанавливает данные алиаса игрока.
//
// TODO: поддержать работу алиасов.
func (s *Storage) SetUserAliasInfo(info storage.UserAliasInfo) bool {
	return false
}

// Получает данные алиаса игрока по названию.
//
// TODO: поддержать работу алиасов.
func (s *Storage) GetUserAliasInfo(user, alias string) (storage.UserAliasInfo, bool) {
	return storage.UserAliasInfo{}, false
}

// Получает данные алиасов игрока.
//
// TODO: поддержать работу алиасов.
func (s *Storage) GetUserAliasInfoList(user string) []storage.UserAliasInfo {
	return nil
}

// DelUserAliasInfo - удаляет данные алиаса игрока.
//
// TODO: поддержать работу алиасов.
func (s *Storage) DelUserAliasInfo(user, alias string) bool {
	return false
}
