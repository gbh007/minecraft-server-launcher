package memoryStorage

import (
	"app/storage"
)

// defaultBuffDuration - стандартная длительность баффов.
const defaultBuffDuration = 60 * 60

// cheatBuff - все полезные баффы.
var cheatBuff = []storage.BuffInfo{
	{Code: "cheat", MinecraftCode: "minecraft:absorption", Duration: defaultBuffDuration, Level: 4},
	{Code: "cheat", MinecraftCode: "minecraft:conduit_power", Duration: defaultBuffDuration, Level: 0},
	{Code: "cheat", MinecraftCode: "minecraft:dolphins_grace", Duration: defaultBuffDuration, Level: 10},
	{Code: "cheat", MinecraftCode: "minecraft:fire_resistance", Duration: defaultBuffDuration, Level: 0},
	{Code: "cheat", MinecraftCode: "minecraft:haste", Duration: defaultBuffDuration, Level: 1},
	{Code: "cheat", MinecraftCode: "minecraft:health_boost", Duration: defaultBuffDuration, Level: 4},
	{Code: "cheat", MinecraftCode: "minecraft:hero_of_the_village", Duration: defaultBuffDuration, Level: 255},
	{Code: "cheat", MinecraftCode: "minecraft:invisibility", Duration: defaultBuffDuration, Level: 255},
	{Code: "cheat", MinecraftCode: "minecraft:jump_boost", Duration: defaultBuffDuration, Level: 2},
	// {Code: "cheat", MinecraftCode: "minecraft:luck", Duration: defaultDuration, Level: 255},
	{Code: "cheat", MinecraftCode: "minecraft:night_vision", Duration: defaultBuffDuration, Level: 0},
	{Code: "cheat", MinecraftCode: "minecraft:regeneration", Duration: defaultBuffDuration, Level: 1},
	{Code: "cheat", MinecraftCode: "minecraft:resistance", Duration: defaultBuffDuration, Level: 3},
	{Code: "cheat", MinecraftCode: "minecraft:saturation", Duration: defaultBuffDuration, Level: 2},
	// {Code: "cheat", MinecraftCode: "minecraft:slow_falling", Duration: defaultDuration, Level: 0},
	{Code: "cheat", MinecraftCode: "minecraft:speed", Duration: defaultBuffDuration, Level: 5},
	{Code: "cheat", MinecraftCode: "minecraft:strength", Duration: defaultBuffDuration, Level: 1},
	{Code: "cheat", MinecraftCode: "minecraft:water_breathing", Duration: defaultBuffDuration, Level: 0},
}

// defaultBuff - упрощенные баффы.
var defaultBuff = []storage.BuffInfo{
	{Code: "default", MinecraftCode: "minecraft:absorption", Duration: defaultBuffDuration, Level: 4},
	{Code: "default", MinecraftCode: "minecraft:fire_resistance", Duration: defaultBuffDuration, Level: 0},
	{Code: "default", MinecraftCode: "minecraft:invisibility", Duration: defaultBuffDuration, Level: 255},
	{Code: "default", MinecraftCode: "minecraft:night_vision", Duration: defaultBuffDuration, Level: 255},
}
