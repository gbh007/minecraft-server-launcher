package memoryStorage

import (
	"app/storage"
	"sync"
)

// Storage - хранилище в оперативной памяти.
//
// TODO: сделать защиту от изменения данных.
type Storage struct {
	// Данные телепортов игроков
	userTPs map[string]map[string]storage.UserTPInfo
	// Нужен для синхронизации запросов данных
	mutex *sync.RWMutex
}

func New() *Storage {
	return &Storage{
		userTPs: make(map[string]map[string]storage.UserTPInfo),
		mutex:   &sync.RWMutex{},
	}
}
