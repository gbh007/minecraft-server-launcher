package memoryStorage

import "app/storage"

// SetUserTPInfo - устанавливает данные телепорта игрока
func (s *Storage) SetUserTPInfo(info storage.UserTPInfo) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if _, found := s.userTPs[info.User]; !found {
		s.userTPs[info.User] = make(map[string]storage.UserTPInfo)
	}

	s.userTPs[info.User][info.Alias] = info

	return true
}

// Получает данные телепорта игрока по названию
func (s *Storage) GetUserTPInfo(user, alias string) (storage.UserTPInfo, bool) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	if _, found := s.userTPs[user]; !found {
		return storage.UserTPInfo{}, false
	}

	info, found := s.userTPs[user][alias]

	return info, found
}

// Получает данные телепортов игрока
func (s *Storage) GetUserTPInfos(user string) []storage.UserTPInfo {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	if _, found := s.userTPs[user]; !found {
		return nil
	}

	res := make([]storage.UserTPInfo, 0, len(s.userTPs[user]))

	for _, info := range s.userTPs[user] {
		res = append(res, info)
	}

	return res
}

// DelUserTPInfo - удаляет данные телепорта игрока.
func (s *Storage) DelUserTPInfo(user, alias string) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if _, found := s.userTPs[user]; !found {
		return false
	}

	_, found := s.userTPs[user][alias]

	delete(s.userTPs[user], alias)

	return found
}
