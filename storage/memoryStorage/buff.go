package memoryStorage

import "app/storage"

// GetBuffInfo - получает данные эффекта по внутреннему коду.
func (s *Storage) GetBuffInfo(code string) []storage.BuffInfo {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	switch code {
	case "default":
		return defaultBuff
	case "cheat":
		return cheatBuff
	}

	return nil
}

// GetBuffListInfo - получает данные эффектов.
func (s *Storage) GetBuffListInfo() []storage.BuffShortInfo {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	return []storage.BuffShortInfo{
		{
			Code:  "default",
			Count: int64(len(defaultBuff)),
		},
		{
			Code:  "cheat",
			Count: int64(len(cheatBuff)),
		},
	}
}
