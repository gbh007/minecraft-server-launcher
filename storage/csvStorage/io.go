package csvStorage

import (
	"encoding/csv"
	"errors"
	"os"
)

func stringsFromCSV(filename string) ([][]string, error) {
	file, err := os.Open(filename)
	if errors.Is(err, os.ErrNotExist) {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}
	defer file.Close()

	return csv.NewReader(file).ReadAll()
}

func stringsToCSV(filename string, records [][]string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	return csv.NewWriter(file).WriteAll(records)
}
