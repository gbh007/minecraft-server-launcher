package csvStorage

import (
	"app/storage"
	"errors"
	"fmt"
	"strings"
)

func userAliasInfoToStrings(info storage.UserAliasInfo) []string {
	out := make([]string, 4)

	out[0] = info.User
	out[1] = info.Alias
	out[2] = info.Cmd

	if len(info.Args) > 0 {
		out[3] = strings.Join(info.Args, " ")
	}

	return out
}

func userAliasInfoFromStrings(in []string) (*storage.UserAliasInfo, error) {
	if len(in) != 4 {
		return nil, errors.New("not valid input len")
	}

	info := &storage.UserAliasInfo{
		User:  in[0],
		Alias: in[1],
		Cmd:   in[2],
	}

	if in[3] != "" {
		info.Args = strings.Split(in[3], " ")
	}

	return info, nil
}

func userAliasInfoToCSV(infos []storage.UserAliasInfo) error {
	out := make([][]string, 0, len(infos))

	for _, info := range infos {
		out = append(out, userAliasInfoToStrings(info))
	}

	return stringsToCSV(userAliasFilename, out)
}

func userAliasInfoFromCSV() ([]storage.UserAliasInfo, error) {
	input, err := stringsFromCSV(userAliasFilename)
	if err != nil {
		return nil, err
	}

	out := make([]storage.UserAliasInfo, 0, len(input))

	for index, in := range input {
		info, err := userAliasInfoFromStrings(in)
		if err != nil {
			return nil, fmt.Errorf("row %d: %w", index, err)
		}

		out = append(out, *info)
	}

	return out, nil
}
