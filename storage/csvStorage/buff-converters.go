package csvStorage

import (
	"app/storage"
	"errors"
	"fmt"
	"strconv"
)

func buffInfoToStrings(info storage.BuffInfo) []string {
	out := make([]string, 4)

	out[0] = info.Code
	out[1] = info.MinecraftCode
	out[2] = strconv.FormatInt(info.Duration, 10)
	out[3] = strconv.FormatInt(info.Level, 10)

	return out
}

func buffInfoFromStrings(in []string) (*storage.BuffInfo, error) {
	if len(in) != 4 {
		return nil, errors.New("not valid input len")
	}

	info := &storage.BuffInfo{
		Code:          in[0],
		MinecraftCode: in[1],
	}

	var err error

	info.Duration, err = strconv.ParseInt(in[2], 10, 64)
	if err != nil {
		return nil, err
	}

	info.Level, err = strconv.ParseInt(in[3], 10, 64)
	if err != nil {
		return nil, err
	}

	return info, nil
}

func BuffInfoToCSV(infos []storage.BuffInfo) error {
	out := make([][]string, 0, len(infos))

	for _, info := range infos {
		out = append(out, buffInfoToStrings(info))
	}

	return stringsToCSV(buffInfoFilename, out)
}

func buffInfoFromCSV() ([]storage.BuffInfo, error) {
	input, err := stringsFromCSV(buffInfoFilename)
	if err != nil {
		return nil, err
	}

	out := make([]storage.BuffInfo, 0, len(input))

	for index, in := range input {
		info, err := buffInfoFromStrings(in)
		if err != nil {
			return nil, fmt.Errorf("row %d: %w", index, err)
		}

		out = append(out, *info)
	}

	return out, nil
}
