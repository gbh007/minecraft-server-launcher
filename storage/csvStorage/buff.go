package csvStorage

import (
	"app/storage"
	"log"
)

// GetBuffInfo - получает данные эффекта по внутреннему коду.
func (s *Storage) GetBuffInfo(code string) []storage.BuffInfo {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	unfiltered, err := buffInfoFromCSV()
	if err != nil {
		log.Println(err)

		return nil
	}

	out := make([]storage.BuffInfo, 0)

	for _, info := range unfiltered {
		if info.Code != code {
			continue
		}

		out = append(out, info)
	}

	return out
}

// GetBuffListInfo - получает данные эффектов.
func (s *Storage) GetBuffListInfo() []storage.BuffShortInfo {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	unfiltered, err := buffInfoFromCSV()
	if err != nil {
		log.Println(err)

		return nil
	}

	tmp := make(map[string]int64)

	for _, info := range unfiltered {
		tmp[info.Code]++
	}

	out := make([]storage.BuffShortInfo, 0, len(tmp))

	for c, v := range tmp {
		out = append(out, storage.BuffShortInfo{
			Code:  c,
			Count: v,
		})
	}

	return out
}
