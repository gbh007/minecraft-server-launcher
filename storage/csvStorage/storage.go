package csvStorage

import (
	"sync"
)

// Storage - хранилище в файлах csv.
type Storage struct {
	// Нужен для синхронизации запросов данных
	mutex *sync.RWMutex
}

func New() *Storage {
	return &Storage{
		mutex: &sync.RWMutex{},
	}
}
