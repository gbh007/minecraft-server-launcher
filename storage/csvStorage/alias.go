package csvStorage

import (
	"app/storage"
	"log"
)

// SetUserAliasInfo - устанавливает данные алиаса игрока.
func (s *Storage) SetUserAliasInfo(info storage.UserAliasInfo) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	raw, err := userAliasInfoFromCSV()
	if err != nil {
		log.Println(err)

		return false
	}

	replaced := false

	for index, oldInfo := range raw {
		if oldInfo.User != info.User || oldInfo.Alias != info.Alias {
			continue
		}

		raw[index] = info
		replaced = true
	}

	if !replaced {
		raw = append(raw, info)
	}

	err = userAliasInfoToCSV(raw)
	if err != nil {
		log.Println(err)

		return false
	}

	return true
}

// Получает данные алиаса игрока по названию.
func (s *Storage) GetUserAliasInfo(user, alias string) (storage.UserAliasInfo, bool) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	unfiltered, err := userAliasInfoFromCSV()
	if err != nil {
		log.Println(err)

		return storage.UserAliasInfo{}, false
	}

	for _, info := range unfiltered {
		if info.User != user || info.Alias != alias {
			continue
		}

		return info, true
	}

	return storage.UserAliasInfo{}, false
}

// Получает данные алиасов игрока.
func (s *Storage) GetUserAliasInfoList(user string) []storage.UserAliasInfo {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	unfiltered, err := userAliasInfoFromCSV()
	if err != nil {
		log.Println(err)

		return nil
	}

	out := make([]storage.UserAliasInfo, 0)

	for _, info := range unfiltered {
		if info.User != user {
			continue
		}

		out = append(out, info)
	}

	return out
}

// DelUserAliasInfo - удаляет данные алиаса игрока.
func (s *Storage) DelUserAliasInfo(user, alias string) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	raw, err := userAliasInfoFromCSV()
	if err != nil {
		log.Println(err)

		return false
	}

	tmp := make([]storage.UserAliasInfo, 0, len(raw))

	for index := range raw {
		info := raw[index]
		if info.User == user && info.Alias == alias {
			continue
		}

		tmp = append(tmp, info)
	}

	deleted := len(raw) > len(tmp)

	err = userAliasInfoToCSV(tmp)
	if err != nil {
		log.Println(err)

		return false
	}

	return deleted
}
