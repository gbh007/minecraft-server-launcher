package csvStorage

import (
	"app/storage"
	"log"
)

// SetUserTPInfo - устанавливает данные телепорта игрока
func (s *Storage) SetUserTPInfo(info storage.UserTPInfo) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	raw, err := userTPInfoFromCSV()
	if err != nil {
		log.Println(err)

		return false
	}

	replaced := false

	for index, oldInfo := range raw {
		if oldInfo.User != info.User || oldInfo.Alias != info.Alias {
			continue
		}

		raw[index] = info
		replaced = true
	}

	if !replaced {
		raw = append(raw, info)
	}

	err = userTPInfoToCSV(raw)
	if err != nil {
		log.Println(err)

		return false
	}

	return true
}

// Получает данные телепорта игрока по названию
func (s *Storage) GetUserTPInfo(user, alias string) (storage.UserTPInfo, bool) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	unfiltered, err := userTPInfoFromCSV()
	if err != nil {
		log.Println(err)

		return storage.UserTPInfo{}, false
	}

	var globalTp *storage.UserTPInfo

	for index := range unfiltered {
		info := unfiltered[index]

		switch {

		// Не совпало название
		case info.Alias != alias:
			continue

		// Пользователь и название совпали
		case info.User == user:
			return info, true

		// Глобальный телепорт
		case info.User == storage.GlobalTpUser:
			globalTp = &info

		}
	}

	// Найден глобальный телепорт
	if globalTp != nil {
		return *globalTp, true
	}

	return storage.UserTPInfo{}, false
}

// Получает данные телепортов игрока
func (s *Storage) GetUserTPInfos(user string) []storage.UserTPInfo {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	unfiltered, err := userTPInfoFromCSV()
	if err != nil {
		log.Println(err)

		return nil
	}

	out := make([]storage.UserTPInfo, 0)

	for _, info := range unfiltered {
		if info.User != user {
			continue
		}

		out = append(out, info)
	}

	return out
}

// DelUserTPInfo - удаляет данные телепорта игрока.
func (s *Storage) DelUserTPInfo(user, alias string) bool {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	raw, err := userTPInfoFromCSV()
	if err != nil {
		log.Println(err)

		return false
	}

	tmp := make([]storage.UserTPInfo, 0, len(raw))

	for index := range raw {
		info := raw[index]
		if info.User == user && info.Alias == alias {
			continue
		}

		tmp = append(tmp, info)
	}

	deleted := len(raw) > len(tmp)

	err = userTPInfoToCSV(tmp)
	if err != nil {
		log.Println(err)

		return false
	}

	return deleted
}
