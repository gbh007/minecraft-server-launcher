package csvStorage

import (
	"app/storage"
	"errors"
	"fmt"
	"strconv"
)

func userTPInfoToStrings(info storage.UserTPInfo) []string {
	out := make([]string, 6)

	out[0] = info.User
	out[1] = info.Alias
	out[2] = info.Dimension
	out[3] = strconv.FormatFloat(info.X, 'f', 2, 64)
	out[4] = strconv.FormatFloat(info.Y, 'f', 2, 64)
	out[5] = strconv.FormatFloat(info.Z, 'f', 2, 64)

	return out
}

func userTPInfoFromStrings(in []string) (*storage.UserTPInfo, error) {
	if len(in) != 6 {
		return nil, errors.New("not valid input len")
	}

	info := &storage.UserTPInfo{
		User:      in[0],
		Alias:     in[1],
		Dimension: in[2],
	}

	var err error

	info.X, err = strconv.ParseFloat(in[3], 64)
	if err != nil {
		return nil, err
	}

	info.Y, err = strconv.ParseFloat(in[4], 64)
	if err != nil {
		return nil, err
	}

	info.Z, err = strconv.ParseFloat(in[5], 64)
	if err != nil {
		return nil, err
	}

	return info, nil
}

func userTPInfoToCSV(infos []storage.UserTPInfo) error {
	out := make([][]string, 0, len(infos))

	for _, info := range infos {
		out = append(out, userTPInfoToStrings(info))
	}

	return stringsToCSV(userTpFilename, out)
}

func userTPInfoFromCSV() ([]storage.UserTPInfo, error) {
	input, err := stringsFromCSV(userTpFilename)
	if err != nil {
		return nil, err
	}

	out := make([]storage.UserTPInfo, 0, len(input))

	for index, in := range input {
		info, err := userTPInfoFromStrings(in)
		if err != nil {
			return nil, fmt.Errorf("row %d: %w", index, err)
		}

		out = append(out, *info)
	}

	return out, nil
}
