package storage

// GlobalTpUser - пользователь для глобальных телепортов
const GlobalTpUser = ""

// UserTPInfo - данные телепорта игрока
type UserTPInfo struct {
	// Имя игрока
	User string
	// Название телепорта
	Alias string
	// Координаты
	X, Y, Z float64
	// Название измерения
	Dimension string
}

// BuffInfo - данные эффекта.
type BuffInfo struct {
	// Внутренний код эффекта.
	Code string
	// Внутренний код эффекта в майнкрафте
	MinecraftCode string
	// Продолжительность наложения в секундах,
	// значение меньше 1 накладывает бесконечный эффект.
	Duration int64
	// Уровень эффекта [0-255]
	Level int64
}

// BuffShortInfo - данные эффекта.
type BuffShortInfo struct {
	// Внутренний код эффекта.
	Code string
	// Количество эффектов по коду
	Count int64
}

// UserAliasInfo - данные алиаса (кастомной команды) игрока
type UserAliasInfo struct {
	// Имя игрока.
	User string
	// Название команды.
	Alias string
	// Командное слово (важно, не должно включать "!").
	Cmd string
	// Аргументы.
	Args []string
}
