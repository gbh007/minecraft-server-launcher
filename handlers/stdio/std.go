package stdio

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
)

// StdIO - обработчик стандартного ввода-вывода приложения
type StdIO struct {
	// Сохраненная функция для вызова команд.
	cmd func(string)
}

// RegisterCommander - регистрирует функцию для вызова команд.
func (h *StdIO) RegisterCommander(c func(string)) {
	h.cmd = c
}

// HandleOut - обработчик стандартного потока данных.
func (h *StdIO) HandleOut(s string) {
	fmt.Println("$", s)
}

// HandleErr - обработчик потока ошибок.
func (h *StdIO) HandleErr(s string) {
	log.Println("$", s)
}

// Run - запускает обработчик стандартного ввода приложения.
func (h *StdIO) Run(ctx context.Context) {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		h.cmd(scanner.Text())
	}
}
