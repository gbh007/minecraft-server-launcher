package model

// UserCord - координаты игрока
type UserCord struct {
	// Имя игрока
	Name string
	// Координаты
	X, Y, Z float64
	// Название измерения
	Dimension string
}
