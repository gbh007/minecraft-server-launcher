package bot

import (
	"app/storage"
	"fmt"
	"sort"
)

func (h *Bot) cmdTp(cmd botCmdInput, deep int) {
	switch {
	case len(cmd.Args) == 1 && cmd.Args[0] == "list":
		h.cmdTpAliasList(cmd)

	case len(cmd.Args) == 1:
		h.cmdTpAlias(cmd)

	case len(cmd.Args) == 2 && cmd.Args[0] == "set":
		h.cmdTpAliasSet(cmd)

	case len(cmd.Args) == 2 && cmd.Args[0] == "del":
		h.cmdTpAliasDel(cmd)

	default:
		h.cmdTpCord(cmd)
	}
}

func (h *Bot) cmdTpCord(cmd botCmdInput) {
	// TODO: рефакторинг и актуализация
	var x, y, z int64
	_, err := fmt.Sscanf(cmd.Raw, "!tp %d %d %d", &x, &y, &z)
	if err != nil {
		h.privateMessage(cmd.User, "Неправильный формат\n"+err.Error(), colorTextError)
	} else {
		h.cmd(fmt.Sprintf("/tp %s %d %d %d", cmd.User, x, y, z))
	}
}

func (h *Bot) cmdTpAlias(cmd botCmdInput) {
	alias := cmd.Args[0]

	info, found := h.stor.GetUserTPInfo(cmd.User, alias)

	if !found {
		h.privateMessage(cmd.User, fmt.Sprintf("Нет сохраненного телепорта \"%s\"", alias), colorTextError)
	}

	h.cmd(fmt.Sprintf(
		"/execute in %s run tp %s %.2f %.2f %.2f",
		info.Dimension, cmd.User, info.X, info.Y, info.Z,
	))
}

func (h *Bot) cmdTpAliasList(cmd botCmdInput) {
	playerList := h.stor.GetUserTPInfos(cmd.User)
	globalList := h.stor.GetUserTPInfos(storage.GlobalTpUser)

	if len(playerList) == 0 && len(globalList) == 0 {
		h.privateMessage(cmd.User, "Нет сохраненных телепортов", colorTextWarning)

		return
	}

	sort.Slice(playerList, func(i, j int) bool {
		return playerList[i].Alias < playerList[j].Alias
	})

	sort.Slice(globalList, func(i, j int) bool {
		return globalList[i].Alias < globalList[j].Alias
	})

	h.privateFormattedMessage(cmd.User, prettyTableTPInfo(playerList, globalList))
}

func (h *Bot) cmdTpAliasSet(cmd botCmdInput) {
	alias := cmd.Args[1]

	cord, found := h.corder(cmd.User)
	if !found {
		h.privateMessage(cmd.User, "Не удалось определить координаты", colorTextError)

		return
	}

	tpInfo := storage.UserTPInfo{
		User:      cmd.User,
		Alias:     alias,
		X:         cord.X,
		Y:         cord.Y,
		Z:         cord.Z,
		Dimension: cord.Dimension,
	}

	ok := h.stor.SetUserTPInfo(tpInfo)

	if !ok {
		h.privateMessage(cmd.User, "Не удалось установить телепорт", colorTextError)
	} else {
		h.privateMessage(cmd.User, "Телепорт установлен\n"+prettyTPInfo(tpInfo), colorTextSuccess)
	}
}

func prettyTPInfo(info storage.UserTPInfo) string {
	return fmt.Sprintf(
		"\"%s\" %.2f %.2f %.2f %s",
		info.Alias,
		info.X, info.Y, info.Z,
		info.Dimension,
	)
}

func prettyTPInfoV2(info storage.UserTPInfo) formattedText {
	return formattedText{
		// Color: color_reset,
		Color: color_white,
		Extra: []formattedText{
			{
				Text:       info.Alias,
				Color:      colorTextLink,
				Underlined: true,
				Insertion:  fmt.Sprintf("!tp %s", info.Alias),
			},
			{
				Text:   fmt.Sprintf(" %.2f %.2f %.2f ", info.X, info.Y, info.Z),
				Italic: true,
			},
			{
				Text:  info.Dimension,
				Color: dimensionColor(info.Dimension),
			},
			{
				Text: "\n",
			},
		},
	}
}

func prettyTableTPInfo(playerList, globalList []storage.UserTPInfo) formattedText {
	out := formattedText{
		Text: "\n",
		Extra: []formattedText{
			{
				Text:  "Список телепортов\n",
				Color: color_blue,
				Bold:  true,
			},
			{
				Text:   "\nГлобальные\n",
				Color:  color_blue,
				Italic: true,
			},
		},
	}

	for _, info := range globalList {
		out.Extra = append(out.Extra, prettyTPInfoV2(info))
	}

	out.Extra = append(out.Extra, formattedText{
		Text:   "\nЛокальные\n",
		Color:  color_blue,
		Italic: true,
	})

	for _, info := range playerList {
		out.Extra = append(out.Extra, prettyTPInfoV2(info))
	}

	return out
}

func (h *Bot) cmdTpAliasDel(cmd botCmdInput) {
	alias := cmd.Args[1]

	ok := h.stor.DelUserTPInfo(cmd.User, alias)

	if !ok {
		h.privateMessage(cmd.User, "Не удалось удалить телепорт", colorTextError)
	} else {
		h.privateMessage(cmd.User, fmt.Sprintf("Телепорт \"%s\" удален", alias), colorTextSuccess)
	}
}
