package bot

import (
	"app/storage"
	"fmt"
	"sort"
	"strings"
)

func (h *Bot) cmdAlias(cmd botCmdInput, deep int) {
	switch {
	case len(cmd.Args) == 1 && cmd.Args[0] == "list":
		h.cmdAliasList(cmd)

	case len(cmd.Args) >= 3 && cmd.Args[0] == "set":
		h.cmdAliasSet(cmd)

	case len(cmd.Args) == 2 && cmd.Args[0] == "del":
		h.cmdAliasDel(cmd)

	default:
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)
	}
}

func (h *Bot) cmdAliasList(cmd botCmdInput) {
	playerList := h.stor.GetUserAliasInfoList(cmd.User)

	if len(playerList) == 0 {
		h.privateMessage(cmd.User, "Нет сохраненных кастомных команд", colorTextWarning)

		return
	}

	sort.Slice(playerList, func(i, j int) bool {
		return playerList[i].Alias < playerList[j].Alias
	})

	h.privateFormattedMessage(cmd.User, prettyTableAliasInfo(playerList))
}

func (h *Bot) cmdAliasSet(cmd botCmdInput) {
	info := storage.UserAliasInfo{
		User:  cmd.User,
		Alias: cmd.Args[1],
		Cmd:   cmd.Args[2],
		Args:  cmd.Args[3:],
	}

	ok := h.stor.SetUserAliasInfo(info)

	if !ok {
		h.privateMessage(cmd.User,
			"Не удалось установить кастомную команду",
			colorTextError)
	} else {
		h.privateFormattedMessage(cmd.User, formattedText{
			Text: "\n",
			Extra: []formattedText{
				{
					Text:  "Кастомная команда установлена\n",
					Color: colorTextSuccess,
					Bold:  true,
				},
				prettyAliasInfo(info),
			},
		})
	}
}

func prettyAliasInfo(info storage.UserAliasInfo) formattedText {
	tmp := []string{info.Cmd}
	tmp = append(tmp, info.Args...)
	return formattedText{
		// Color: color_reset,
		Color: color_white,
		Extra: []formattedText{
			{
				Text:       info.Alias,
				Color:      colorTextLink,
				Underlined: true,
				Insertion:  fmt.Sprintf("!%s", info.Alias),
			},
			{
				Text: " - ",
			},
			{
				Text:   strings.Join(tmp, " "),
				Italic: true,
			},
			{
				Text: "\n",
			},
		},
	}
}

func prettyTableAliasInfo(playerList []storage.UserAliasInfo) formattedText {
	out := formattedText{
		Text: "\n",
		Extra: []formattedText{
			{
				Text:  "Список кастомных команд\n",
				Color: color_blue,
				Bold:  true,
			},
		},
	}

	for _, info := range playerList {
		out.Extra = append(out.Extra, prettyAliasInfo(info))
	}

	return out
}

func (h *Bot) cmdAliasDel(cmd botCmdInput) {
	alias := cmd.Args[1]

	ok := h.stor.DelUserAliasInfo(cmd.User, alias)

	if !ok {
		h.privateMessage(cmd.User,
			"Не удалось удалить кастомную команду",
			colorTextError)
	} else {
		h.privateMessage(cmd.User,
			fmt.Sprintf("Кастомная команда \"%s\" удалена", alias),
			colorTextSuccess)
	}
}
