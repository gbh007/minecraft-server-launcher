package bot

import "testing"

func TestArraySplitter(t *testing.T) {
	out1 := arraySplitter([]string{"!!!", "!!!", "!!!", "!!!", "!!!", "!!!", "!!!", "!!!"}, "!!!")
	if len(out1) != 0 {
		t.FailNow()
	}

	// TODO: написать тесты на сравнение получше
	out2 := arraySplitter([]string{"tp", "set", "tmp", "!!!", "home", "!!!", "buff"}, "!!!")
	t.Log(out2)
	if len(out2) != 3 {
		t.FailNow()
	}
}
