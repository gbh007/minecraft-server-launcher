package bot

import "fmt"

func (h *Bot) fillCMD() {
	commands := []botCmd{

		{
			Code: "purge",
			Handler: func(cmd botCmdInput, deep int) {
				h.cmd(fmt.Sprintf("/effect clear %s", cmd.User))
			},
			Help: []string{"!purge - великое очищение"},
		},

		{
			Code: "harakiri",
			Handler: func(cmd botCmdInput, deep int) {
				h.cmd(fmt.Sprintf("/kill %s", cmd.User))
			},
			Help: []string{"!harakiri - вознесение"},
		},

		{
			Code: "spectra",
			Handler: func(cmd botCmdInput, deep int) {
				h.cmd(fmt.Sprintf("/gamemode spectator %s", cmd.User))
			},
			Help: []string{"!spectra - стань призраком"},
		},

		{
			Code: "surv",
			Handler: func(cmd botCmdInput, deep int) {
				h.cmd(fmt.Sprintf("/gamemode survival %s", cmd.User))
			},
			Help: []string{"!surv - стань самураем"},
		},

		{
			Code:    "buff",
			Handler: h.cmdBuff,
			Help: []string{
				"!buff list - список великих благословений админом",
				"!buff {name} - великое благословение админом",
			},
		},

		{
			Code:    "tp",
			Handler: h.cmdTp,
			Help: []string{
				"!tp {X} {Y} {Z} - точная великая телепортация",
				"!tp list - список великих телепортаций",
				"!tp del {name} - удалить великую телепортацию",
				"!tp set {name} - запомнить великую телепортацию",
				"!tp {name} - великая телепортация",
			},
		},

		{
			Code:    "alias",
			Handler: h.cmdAlias,
			Help: []string{
				"!alias list - список великой воли",
				"!alias del {name} - удалить великую волю",
				"!alias set {name} {command} [{args}]- запомнить великую волю",
				"!{alias_name} [{args}]- вызвать великую волю",
			},
		},

		{
			Code:    "chain",
			Handler: h.cmdChain,
			Help: []string{
				"!chain {sep} {cmd} [{args}] {sep} {cmd} [{args}] - великая последовательность",
			},
		},

		{
			Code:    "echo",
			Handler: h.cmdEcho,
			Help: []string{
				"!echo {args} - великое слово",
			},
		},

		{
			Code:    "if",
			Handler: h.cmdIf,
			Help: []string{
				"!if [reverse] <tp|alias|buff> {name} {cmd} [{args}] - великий вопрос",
			},
		},

		{
			Code:    "sleep",
			Handler: h.cmdSleep,
			Help: []string{
				"!sleep {sec} - великое ожидание",
			},
		},

		{
			Code:    "help",
			Handler: h.cmdHelp,
		},
	}

	for _, cmd := range commands {
		h.commands[cmd.Code] = cmd
	}
}
