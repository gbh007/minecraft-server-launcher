package bot

import (
	"fmt"
	"sort"
	"strings"
)

func (h *Bot) cmdBuff(cmd botCmdInput, deep int) {
	switch {
	case len(cmd.Args) == 1 && cmd.Args[0] == "list":
		h.cmdBuffList(cmd)

	default:
		h.cmdBuffApply(cmd)
	}
}

func (h *Bot) cmdBuffApply(cmd botCmdInput) {
	code := "default"

	if len(cmd.Args) == 1 {
		code = cmd.Args[0]
	}

	buffs := h.stor.GetBuffInfo(code)

	if len(buffs) == 0 {
		h.privateMessage(cmd.User,
			fmt.Sprintf("Великое благословение \"%s\" не найдено", code),
			colorTextError)

		return
	}

	h.buffer(cmd.User, buffs)

	h.privateMessage(cmd.User,
		fmt.Sprintf("Вы благословенны \"%s\"", code),
		colorTextSuccess)
}

func (h *Bot) cmdBuffList(cmd botCmdInput) {
	list := h.stor.GetBuffListInfo()

	if len(list) == 0 {
		h.privateMessage(cmd.User,
			"Великих благословений не найдено",
			colorTextWarning)

		return
	}

	out := make([]string, len(list))

	for _, info := range list {
		out = append(out, fmt.Sprintf("\"%s\" - %d шт", info.Code, info.Count))
	}

	sort.Strings(out)

	h.privateMessage(cmd.User, strings.Join(out, "\n"), colorTextCommon)
}
