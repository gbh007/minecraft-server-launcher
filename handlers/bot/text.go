package bot

// formattedText - форматированный текст для вывода на сервере.
type formattedText struct {
	// Текст для вывода.
	Text string `json:"text"`
	// Цвет текста (палитра цветов майнкрафта).
	Color string `json:"color,omitempty"`

	// Делает текст полужирным.
	Bold bool `json:"bold,omitempty"`
	//Делает текст курсивным.
	Italic bool `json:"italic,omitempty"`
	// Делает текст подчёркнутым.
	Underlined bool `json:"underlined,omitempty"`
	// Делает текст зачёркнутым.
	Strikethrough bool `json:"strikethrough,omitempty"`
	// Заставляет символы в тексте постоянно изменяться.
	Obfuscated bool `json:"obfuscated,omitempty"`

	// Когда игрок нажимает на текст при помощи Shift+ЛКМ, строка этого элемента будет вставлена в чат.
	Insertion string `json:"insertion,omitempty"`

	// Дочерние элементы.
	Extra []formattedText `json:"extra,omitempty"`
}

// Палитра майнкратфа.
// reset - сбрасывает родительский цвет.
const (
	color_black        = "black"
	color_dark_blue    = "dark_blue"
	color_dark_green   = "dark_green"
	color_dark_aqua    = "dark_aqua"
	color_dark_red     = "dark_red"
	color_dark_purple  = "dark_purple"
	color_gold         = "gold"
	color_gray         = "gray"
	color_dark_gray    = "dark_gray"
	color_blue         = "blue"
	color_green        = "green"
	color_aqua         = "aqua"
	color_red          = "red"
	color_light_purple = "light_purple"
	color_yellow       = "yellow"
	color_white        = "white"
	color_reset        = "reset" // FIXME: есть проблемы с поддержкой в minecraft 1.20.4
)

// Основные цвета.
const (
	colorTextError   = color_red
	colorTextSuccess = color_green
	colorTextWarning = color_yellow
	colorTextCommon  = color_white
	colorTextLink    = color_aqua
)

// Измерения в майнкрафте.
const (
	dimension_overworld  = "minecraft:overworld"
	dimension_the_nether = "minecraft:the_nether"
	dimension_the_end    = "minecraft:the_end"
)

func dimensionColor(d string) string {
	switch d {
	case dimension_overworld:
		return color_dark_green
	case dimension_the_nether:
		return color_dark_red
	case dimension_the_end:
		return color_yellow
	}

	return color_red
}
