package bot

import (
	"sort"
	"strings"
)

// botCmd - команда обрабатываемая ботом
type botCmd struct {
	// Код команды (первое слово)
	Code string
	// Обработчик команды
	Handler func(cmd botCmdInput, deep int)
	// Данные для помощи
	Help []string
}

// handleCMD - обработчик команды.
func (h *Bot) handleCMD(cmd botCmdInput, deep int) {
	// Защита от рекурсии
	if deep > 10 {
		h.privateMessage(cmd.User, "Слишком сложная команда!", colorTextError)

		return
	}

	// У пользователя есть алиас, пытаемся его выполнить
	userAlias, found := h.stor.GetUserAliasInfo(cmd.User, cmd.Cmd)
	if found {
		userCmd := botCmdInput{
			User: cmd.User,
			Raw:  cmd.Raw,
			Cmd:  userAlias.Cmd,
			Args: append(userAlias.Args, cmd.Args...), // Собираем все аргументы
		}

		h.handleCMD(userCmd, deep+1)

		return
	}

	cmdInfo, found := h.commands[cmd.Cmd]
	if !found {
		h.privateMessage(cmd.User, "Неизвестная команда, !help ваш помощник команд", colorTextError)

		return
	}

	cmdInfo.Handler(cmd, deep)
}

func (h *Bot) cmdHelp(cmd botCmdInput, deep int) {
	out := []string{}

	for _, cmdInfo := range h.commands {
		if len(cmdInfo.Help) == 0 {
			continue
		}

		out = append(out, cmdInfo.Help...)
	}

	if len(out) > 0 {
		sort.Strings(out)

		h.privateMessage(cmd.User, strings.Join(out, "\n"), colorTextCommon)
	}
}
