package bot

import (
	"app/handlers/model"
	"app/storage"
	"log"
	"strings"
)

const (
	// botCmdPreffix - префикс для фильтрации ввода пользователя
	botCmdPreffix = `[Server thread/INFO]: [Not Secure] <`
)

// Bot - обработчик бот.
type Bot struct {
	// Сохраненная функция для вызова команд.
	cmd func(string)
	// Сохраненная функция для получения координат.
	corder func(string) (model.UserCord, bool)
	// Включен режим читов.
	//
	// TODO: сделать CMS и управление через нее.
	Cheats bool
	// Команды для бота.
	commands map[string]botCmd
	// Хранилище данных
	stor storage.Storage
}

// New - создает новый экземпляр бота.
func New(stor storage.Storage) *Bot {
	bot := &Bot{
		commands: make(map[string]botCmd),
		stor:     stor,
	}

	bot.fillCMD()

	return bot
}

// RegisterCommander - регистрирует функцию для вызова команд.
func (h *Bot) RegisterCommander(c func(string)) {
	h.cmd = c
}

// RegisterCorder - регистрирует функцию получения координат.
func (h *Bot) RegisterCorder(c func(string) (model.UserCord, bool)) {
	h.corder = c
}

// HandleOut - обработчик стандартного потока данных.
func (h *Bot) HandleOut(s string) {
	if !strings.Contains(s, botCmdPreffix) {
		return
	}

	// TODO: произвести рефакторинг с оптимизацией

	tmp := strings.Split(s, botCmdPreffix)

	if len(tmp) != 2 {
		return
	}

	s = tmp[1]

	sepIndex := strings.Index(s, ">")
	if sepIndex == 0 || sepIndex+1 >= len(s) {
		return
	}

	userName := s[:sepIndex]
	cmd := strings.TrimSpace(s[sepIndex+1:])

	if !strings.HasPrefix(cmd, "!") || len(cmd) < 2 {
		return
	}

	log.Printf("user:%s cmd:%s\n", userName, cmd)

	parsedCmd := botCmdInputFromRaw(cmd, userName)

	go h.handleCMD(parsedCmd, 0)
}

// HandleErr - обработчик потока ошибок.
func (h *Bot) HandleErr(s string) {}
