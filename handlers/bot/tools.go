package bot

import (
	"app/storage"
	"encoding/json"
	"fmt"
)

// buffer - производит наложение указанных эффектов на пользователя.
func (h *Bot) buffer(user string, buffs []storage.BuffInfo) {
	for _, buff := range buffs {
		d := "infinite"
		if buff.Duration > 0 {
			d = fmt.Sprint(buff.Duration)
		}

		h.cmd(fmt.Sprintf("/effect give %s %s %s %d true", user, buff.MinecraftCode, d, buff.Level))
	}
}

// privateMessage - пишет сообщение пользователю в личку.
func (h *Bot) privateMessage(user string, text string, color string) {
	raw, _ := json.Marshal(formattedText{
		Text:  text + "\n",
		Color: color,
	})

	h.cmd(fmt.Sprintf("/tellraw %s %s", user, string(raw)))
}

// privateFormattedMessage - пишет форматированное сообщение пользователю в личку.
func (h *Bot) privateFormattedMessage(user string, text formattedText) {
	raw, _ := json.Marshal(text)

	h.cmd(fmt.Sprintf("/tellraw %s %s", user, string(raw)))
}
