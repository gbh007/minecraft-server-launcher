package bot

import (
	"strconv"
	"time"
)

func (h *Bot) cmdSleep(cmd botCmdInput, deep int) {
	if len(cmd.Args) != 1 {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	v, err := strconv.ParseInt(cmd.Args[0], 10, 64)
	if err != nil {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	time.Sleep(time.Second * time.Duration(v))
}
