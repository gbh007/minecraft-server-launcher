package bot

import "strings"

func (h *Bot) cmdEcho(cmd botCmdInput, deep int) {
	if len(cmd.Args) == 0 {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	h.privateMessage(cmd.User, strings.Join(cmd.Args, " "), color_gold)
}
