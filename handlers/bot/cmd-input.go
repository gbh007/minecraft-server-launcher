package bot

import "strings"

// botCmdInput - входящая команда боту.
type botCmdInput struct {
	// Пользователь который ввел команду.
	User string
	// Сырая команда.
	//
	// TODO: удалить, неактуальна.
	Raw string
	// Командное слово.
	Cmd string
	// Аргументы.
	Args []string
}

func botCmdInputFromRaw(raw, user string) botCmdInput {
	cmd := botCmdInput{
		Raw:  raw,
		User: user,
	}

	for _, arg := range strings.Split(strings.TrimPrefix(raw, "!"), " ") {
		// Пропуски и т.п.не имеют значения
		if arg == "" {
			continue
		}

		// Не была установлена команда
		if cmd.Cmd == "" {
			cmd.Cmd = arg

			continue
		}

		cmd.Args = append(cmd.Args, arg)
	}

	return cmd
}
