package bot

func (h *Bot) cmdChain(cmd botCmdInput, deep int) {
	if len(cmd.Args) < 4 {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	cmds := make([]botCmdInput, 0)
	sep := cmd.Args[0]

	for _, v := range arraySplitter(cmd.Args[1:], sep) {
		cmds = append(cmds, botCmdInput{
			User: cmd.User,
			Raw:  cmd.Raw,
			Cmd:  v[0],
			Args: v[1:],
		})
	}

	if len(cmds) == 0 {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	for _, subCmd := range cmds {
		h.handleCMD(subCmd, deep+1)
	}
}

func arraySplitter(in []string, sep string) [][]string {
	tmp := [][]string{}

	last := []string{}

	l := len(in)

	for index, v := range in {
		if v != sep {
			last = append(last, v)
		}

		if v == sep || index+1 == l {
			tmp = append(tmp, last)
			last = []string{}
		}

	}

	out := [][]string{}

	for _, v := range tmp {
		if len(v) == 0 {
			continue
		}

		out = append(out, v)
	}

	return out
}
