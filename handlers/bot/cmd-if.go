package bot

func (h *Bot) cmdIf(cmd botCmdInput, deep int) {
	if len(cmd.Args) < 3 || cmd.Args[0] == "reverse" && len(cmd.Args) < 4 {
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	startPos := 0
	reverse := false

	if cmd.Args[0] == "reverse" {
		startPos = 1
		reverse = true
	}

	compareType := cmd.Args[startPos]
	startPos++
	compareValue := cmd.Args[startPos]
	startPos++

	// Проверка на невыполнение
	switch compareType {

	case "tp":
		_, exists := h.stor.GetUserTPInfo(cmd.User, compareValue)
		if exists == reverse {
			return
		}

	case "alias":
		_, exists := h.stor.GetUserAliasInfo(cmd.User, compareValue)
		if exists == reverse {
			return
		}

	case "buff":
		exists := len(h.stor.GetBuffInfo(compareValue)) != 0
		if exists == reverse {
			return
		}

	default:
		h.privateMessage(cmd.User, "Не верный формат команды", colorTextError)

		return
	}

	cmdToExec := botCmdInput{
		User: cmd.User,
		Raw:  cmd.Raw,
		Cmd:  cmd.Args[startPos],
		Args: cmd.Args[startPos+1:],
	}

	h.handleCMD(cmdToExec, deep+1)
}
