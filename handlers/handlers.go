package handlers

import (
	"app/handlers/bot"
	"app/handlers/cord"
	"app/handlers/stdio"
	"app/storage"
)

// NewCordSearcher - создает новый экземпляр поисковика координат.
func NewCordSearcher() *cord.CordSearcher {
	return cord.New()
}

// NewBot - создает новый экземпляр бота.
func NewBot(stor storage.Storage) *bot.Bot {
	return bot.New(stor)
}

// NewStdIO - создает новый экземпляр обработчика стандартного ввода вывода.
func NewStdIO() *stdio.StdIO {
	return new(stdio.StdIO)
}
