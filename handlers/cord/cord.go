package cord

import (
	"app/handlers/model"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	cordSearchTemplate = `[Server thread/INFO]: %s has the following entity data:`
)

var (
	cordSearchRegExp      = regexp.MustCompile(`\sPos: \[(-?\d+\.?\d*)d, (-?\d+\.?\d*)d, (-?\d+\.?\d*)d\]`)
	dimensionSearchRegExp = regexp.MustCompile(`\sDimension: "([\w:]+?)"`)
)

// CordSearcher - поисковик координат игрока.
type CordSearcher struct {
	// Сохраненная функция для вызова команд.
	cmd func(string)
	// Ник игрока для поиска.
	nameToSearch *string
	// Найденные координаты
	searchedCord chan model.UserCord

	// Нужен для синхронизации поиска по кордам
	mutex *sync.Mutex
}

// New - создает новый экземпляр поисковика координат
func New() *CordSearcher {
	return &CordSearcher{
		searchedCord: make(chan model.UserCord, 5),
		mutex:        &sync.Mutex{},
	}
}

// RegisterCommander - регистрирует функцию для вызова команд.
func (h *CordSearcher) RegisterCommander(c func(string)) {
	h.cmd = c
}

// HandleOut - обработчик стандартного потока данных.
func (h *CordSearcher) HandleOut(s string) {
	// Нет данных для поиска, игнорируем
	if h.nameToSearch == nil {
		return
	}

	// В данных нет координат
	if !strings.Contains(s, fmt.Sprintf(cordSearchTemplate, *h.nameToSearch)) {
		return
	}

	tmp := dimensionSearchRegExp.FindStringSubmatch(s)
	if len(tmp) != 2 {
		return
	}

	cord := model.UserCord{
		Name:      *h.nameToSearch,
		Dimension: tmp[1],
	}

	tmp = cordSearchRegExp.FindStringSubmatch(s)
	if len(tmp) != 4 {
		return
	}

	var err error

	cord.X, err = strconv.ParseFloat(tmp[1], 64)
	if err != nil {
		log.Println(err)

		return
	}

	cord.Y, err = strconv.ParseFloat(tmp[2], 64)
	if err != nil {
		log.Println(err)

		return
	}

	cord.Z, err = strconv.ParseFloat(tmp[3], 64)
	if err != nil {
		log.Println(err)

		return
	}

	h.searchedCord <- cord
}

// HandleErr - обработчик потока ошибок.
func (h *CordSearcher) HandleErr(s string) {}

// Run - запускает обработчик стандартного ввода приложения.
func (h *CordSearcher) Search(name string) (model.UserCord, bool) {
	h.mutex.Lock()
	defer h.mutex.Unlock()

	h.nameToSearch = &name
	h.cmd(fmt.Sprintf("/data get entity %s", name))

	select {
	case cord := <-h.searchedCord:
		h.nameToSearch = nil

		return cord, true

	case <-time.After(time.Second):
		h.nameToSearch = nil
	}

	return model.UserCord{}, false
}
