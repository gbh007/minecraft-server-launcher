package server

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

// ServerHandler - обработчик данных сервера.
type ServerHandler interface {
	// HandleOut - получает на вход строку из стандартного потока вывода сервера.
	HandleOut(string)
	// HandleOut - получает на вход строку из стандартного потока ошибок сервера.
	HandleErr(string)
	// RegisterCommander - регистрирует функцию вызова команд.
	// Вызывается при регистрации хандлера как колбек.
	RegisterCommander(func(string))
}

// ServerConfig - конфигурация сервера.
type ServerConfig struct {
	// Параметры памяти JVM.
	Xmx, Xms string
	// Путь до исполняемого файла JVM.
	JVMPath string
	// Название файла сервера.
	Filename string
}

// Server - контроллер сервера
type Server struct {
	// Оболочка для управления процессом на ОС.
	cmd *exec.Cmd
	// Обвязка для обработки ввода-вывода
	io *serverIO
	// Первичный интерфейс пайпы ввода.
	rawInput io.WriteCloser

	// Зарегистрированные обработчики данных.
	//
	// TODO: обезопасить мультипоточность.
	handlers []ServerHandler
}

// NewServer - создает новый экземпляр сервера.
func NewServer(cfg ServerConfig) *Server {
	cmd := exec.Command(
		cfg.JVMPath,
		fmt.Sprintf("-Xmx%s", cfg.Xmx),
		fmt.Sprintf("-Xms%s", cfg.Xms),
		"-jar",
		cfg.Filename,
		"nogui",
	)

	// Создает пайпу для передачи входных данных,
	// в случае с кастомной потребляется много ресурсов.
	wr, err := cmd.StdinPipe()
	if err != nil {
		log.Fatalln(err)
	}

	// Новая обвязка ввода вывода
	serverIO := newServerIO()
	serverIO.SetWriter(wr)

	// Замена стандартных потоков вывода данных и ошибок
	cmd.Stdout = serverIO.Stdout
	cmd.Stderr = serverIO.Stderr

	return &Server{
		cmd:      cmd,
		io:       serverIO,
		rawInput: wr,
	}
}

// Run - запускает сервер.
func (s *Server) Run(ctx context.Context) error {

	go func() {
		// По окончанию действия необходимо закрыть пайпу
		defer s.rawInput.Close()

	async:
		for {
			select {

			// Обработчик выхода
			case <-ctx.Done():
				s.io.Command("/stop")

				break async

			// Обработчик стандартного потока
			case stdout := <-s.io.StdoutChan:
				if stdout != "" {
					for _, h := range s.handlers {
						h.HandleOut(stdout)
					}
				}

			// Обработчик потока ошибок
			case stderr := <-s.io.StderrChan:
				if stderr != "" {
					for _, h := range s.handlers {
						h.HandleErr(stderr)
					}
				}
			}

		}
	}()

	// Запускаем и дожидаемся завершения
	err := s.cmd.Run()
	if err != nil && !errors.Is(err, os.ErrProcessDone) {
		return err
	}

	return nil
}

// RegisterHandler - регистрирует новый обработчик данных.
func (s *Server) RegisterHandler(h ServerHandler) {
	h.RegisterCommander(s.io.Command)
	s.handlers = append(s.handlers, h)
}
