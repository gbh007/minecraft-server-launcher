package server

import (
	"io"
	"sync"
)

type serverIO struct {
	stdin  io.Writer
	Stdout io.Writer
	Stderr io.Writer

	stdoutBuff *specialWriter
	stderrBuff *specialWriter

	StdoutChan chan string
	StderrChan chan string

	mutex *sync.Mutex

	memGbSize int
}

func newServerIO() *serverIO {

	stdoutChan := make(chan string, 1000)
	stderrChan := make(chan string, 1000)

	stdoutBuff := newSpecialWriter(stdoutChan)
	stderrBuff := newSpecialWriter(stderrChan)

	return &serverIO{
		stdoutBuff: stdoutBuff,
		stderrBuff: stderrBuff,

		Stderr: stderrBuff,
		Stdout: stdoutBuff,

		StdoutChan: stdoutChan,
		StderrChan: stderrChan,

		mutex: &sync.Mutex{},

		memGbSize: 1,
	}
}

func (sio *serverIO) SetWriter(w io.Writer) {
	sio.stdin = w
}

func (sio *serverIO) Command(input string) {
	sio.mutex.Lock()
	defer sio.mutex.Unlock()

	if input != "" {
		_, _ = io.WriteString(sio.stdin, input+"\n")
	}
}
