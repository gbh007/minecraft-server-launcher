package server

import (
	"bytes"
	"strings"
	"sync"
)

type specialWriter struct {
	mutex *sync.Mutex
	ch    chan string
	buff  *bytes.Buffer
}

func newSpecialWriter(ch chan string) *specialWriter {
	return &specialWriter{
		mutex: &sync.Mutex{},
		ch:    ch,
		buff:  &bytes.Buffer{},
	}
}

func (sw *specialWriter) Write(p []byte) (int, error) {
	sw.mutex.Lock()
	defer sw.mutex.Unlock()

	out := ""

	// Обработка случая больших данных, они проходят массивами байт.
	if !bytes.ContainsRune(p, '\n') {
		sw.buff.Write(p)

		return len(p), nil
	}

	if sw.buff.Len() != 0 {
		out += sw.buff.String()
	}

	out += string(p)

	sw.ch <- strings.TrimRight(out, "\n")
	sw.buff.Reset()

	return len(p), nil
}
