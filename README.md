# Лаунчер с ботом для майнкрафт сервера

Создан как обвязка для стандартного сервера майнкрафт с целью расширения возможностей.

## Конфигурация глобальных телепортов

Файл - `user-tp.csv`

Строки вида `,{NAME},{DIMENSION},{X},{Y},{Z}`

Пример

```csv
,hub,minecraft:overworld,0.06,63.00,-99.92
```

## Конфигурация баффов (эффектов)

Файл - `buff-info.csv`

Строки вида `{CODE},{MINECRAFT_CODE},{DURATION_IN_SEC},{LEVEL}`

Пример

```csv
default,minecraft:absorption,3600,4
default,minecraft:fire_resistance,3600,0
default,minecraft:invisibility,3600,255
default,minecraft:night_vision,3600,255
cheat,minecraft:absorption,3600,4
cheat,minecraft:conduit_power,3600,0
cheat,minecraft:dolphins_grace,3600,10
cheat,minecraft:fire_resistance,3600,0
cheat,minecraft:haste,3600,1
cheat,minecraft:health_boost,3600,4
cheat,minecraft:hero_of_the_village,3600,255
cheat,minecraft:invisibility,3600,255
cheat,minecraft:jump_boost,3600,2
cheat,minecraft:night_vision,3600,0
cheat,minecraft:regeneration,3600,1
cheat,minecraft:resistance,3600,3
cheat,minecraft:saturation,3600,2
cheat,minecraft:speed,3600,5
cheat,minecraft:strength,3600,1
cheat,minecraft:water_breathing,3600,0
```

## Заметки (вырезки из логов и команд)

Данные логов

```
[20:20:42] [Server thread/INFO]: [Not Secure] <Snamer> GBH
[20:25:32] [Server thread/INFO]: [Not Secure] [Server] Snamer loh
[17:13:45] [Server thread/INFO]: Snamer[/25.39.203.67:65114] logged in with entity id 13378 at (3047.2913561644104, 65.0, -1170.705866187481)
[17:13:45] [Server thread/INFO]: Snamer joined the game
[17:17:32] [Server thread/INFO]: [MrGBH007: Applied effect Night Vision to Snamer]
[17:27:50] [Server thread/INFO]: [MrGBH007: Teleported MrGBH007 to 231.900000, 64.000000, 178.920000]
[18:27:32] [Server thread/INFO]: MrGBH007 lost connection: Disconnected
[18:27:32] [Server thread/INFO]: MrGBH007 left the game
[22:17:22] [Server thread/INFO]: [MrGBH007: Teleported Snamer to MrGBH007]
```

Команда запуска

> `/usr/lib/jvm/jdk-17/bin/java -Xmx4G -Xms4G -jar server.jar nogui`

Баффы

> `/effect give MrGBH007 minecraft:absorption 60 1 true`

```plain
minecraft:absorption
minecraft:conduit_power
minecraft:dolphins_grace
minecraft:fire_resistance
minecraft:haste
minecraft:health_boost
minecraft:hero_of_the_village
minecraft:invisibility
minecraft:jump_boost
minecraft:luck
minecraft:night_vision
minecraft:regeneration
minecraft:resistance
minecraft:saturation
minecraft:slow_falling
minecraft:speed
minecraft:strength
minecraft:water_breathing
```

Координаты игрока

> `/data get entity MrGBH007`

```
[03:44:57] [Server thread/INFO]: MrGBH007 has the following entity data:
Pos: [115.16495348221817d, 71.0d, 339.18819293615235d],
Dimension: "minecraft:overworld",
```

Перемещение между мирами

> `/execute in minecraft:the_nether run tp MrGBH007 0 130 0`

Измерения

```plain
minecraft:overworld
minecraft:the_nether
minecraft:the_end
```

Многострочный текст

> /tellraw MrGBH007 ["first row\n",{"text": "gold row", "color":"gold" }]

## Пример compose файла

```yaml
version: "3.8"

services:
  server:
    image: openjdk:23-slim-bullseye
    ports:
      - 25565:25565
    working_dir: /minecraft
    entrypoint: /minecraft/server-launcher
    command: --jvm-path=/usr/local/openjdk-23/bin/java -ms 4G
    volumes:
      - ./:/minecraft
```
